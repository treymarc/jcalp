package com.valiantys.jira.plugins.createissueandlink;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Category;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import webwork.action.ResultException;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.AttachmentUtils;
import com.atlassian.jira.web.action.issue.CreateIssueDetails;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.atlassian.jira.web.util.AttachmentException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.user.EntityNotFoundException;
import com.opensymphony.user.UserManager;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Watcher;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.WatcherToAddItem;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * @author VALIANTYS Mathieu AGAR (mathieu.agar@valiantys.com)
 * 
 */
public class CreateIssueAndLinkDetails extends CreateIssueDetails {

	/**
	 * All manager fields are needed to perform create and link operations in
	 * JIRA.
	 */
	private final PermissionManager permissionManager;
	private final IssueLinkManager issueLinkManager;
	private final IssueLinkTypeManager issueLinkTypeManager;
	private final IssueFactory issueFactory;
	private final IssueManager issueManager;

	/**
	 * Logger.
	 */
	private final static Category LOG = Category
			.getInstance(CreateIssueAndLink.class);

	/**
	 * The original issue that is to be linked to.
	 */
	private Issue originalIssue;

	/**
	 * Link type.
	 */
	private IssueLinkType linkType;

	/**
	 * Link direction, values it can take are defined into
	 * CreateIssueAndLinkDetails.properties file.
	 */
	private String linkDirection;

	/**
	 * The type of the selected link (e.g. LINK-1).
	 */
	private String linkTypeName;

	/**
	 * Attachments Path.
	 */
	private String attachmentHome;

	private String parentid;
	private String mappingid;

	/**
	 * Authentication context.
	 */
	private final JiraAuthenticationContext authenticationContext;
	

	/**
	 * @param applicationProperties
	 * @param permissionManager
	 * @param issueLinkManager
	 * @param issueLinkTypeManager
	 * @param issueFactory
	 * @param issueManager
	 * @param issueCreationHelperBean
	 */
	public CreateIssueAndLinkDetails(
			final ApplicationProperties applicationProperties,
			final PermissionManager permissionManager,
			final IssueLinkManager issueLinkManager,
			final IssueLinkTypeManager issueLinkTypeManager,
			final IssueFactory issueFactory, IssueManager issueManager,
			final IssueCreationHelperBean issueCreationHelperBean,
			final JiraAuthenticationContext context,
			final IssueService issueService) {
		super(issueFactory, issueCreationHelperBean, issueService);
		this.permissionManager = permissionManager;
		this.issueLinkManager = issueLinkManager;
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.issueFactory = issueFactory;
		this.issueManager = issueManager;
		this.authenticationContext = context;

		PropertySet propSet = PropertiesManager.getInstance().getPropertySet();
		attachmentHome = propSet.getString("jira.path.attachments");
		LOG.debug("Attachments path : " + attachmentHome);
	}

	protected void validate() throws ResultException {
		
		// Parent issue
		Issue parentIssue = issueManager.getIssueObject(new Long(parentid));
		log.debug("Parent issue key : " + parentIssue);
		log.debug("Link Type Name : " + linkTypeName);
		setOriginalIssue(parentIssue);
		super.validate();
	}

	/**
	 * Action called to create the new issue.
	 * @return the next page : the issue parent detail.
	 * @throws Exception : Exception.
	 */
	protected final String doExecute() throws Exception {

		if (parentid == null || parentid.length() < 1) {
			throw new Exception(
					"CreateIssueAndLink: Missing parameter: parentid is required");
		}
		LOG.debug("CreateIssueAndLinkDetails: parentid is currently:"
				+ parentid);
		LOG.debug("CreateIssueAndLinkDetails: mappingid is currently:"
				+ mappingid);
		// Parent issue
		Issue parentIssue = issueManager.getIssueObject(Long.valueOf(parentid));
		if (parentIssue == null) {
			throw new Exception(
					"CreateIssueAndLink: Unable to find issue with id: "
							+ parentid);
		}
		LOG.debug("CreateIssueAndLinkDetails: parent issue key is "
				+ parentIssue);
		setOriginalIssue(parentIssue);

		LOG.debug("CreateIssueAndLinkDetails: linkTypeName is currently:"
				+ linkTypeName);

		Collection linkTypes = issueLinkTypeManager.getIssueLinkTypes();
		Iterator linkIte = linkTypes.iterator();
		IssueLinkType linkTypeValue = null;

		// --- Looking for Link type
		if (linkTypeName != null) {
			LOG.debug("CreateAndLinkDetail: " + "Looking for link type "
					+ linkTypeName);
			do {
				linkTypeValue = (IssueLinkType) linkIte.next();
			} while (linkIte.hasNext()
					&& (!linkTypeName.equals(linkTypeValue.getOutward()))
					&& (!linkTypeName.equals(linkTypeValue.getInward())));

			if (!linkTypeName.equals(linkTypeValue.getOutward())
					&& !linkTypeName.equals(linkTypeValue.getInward())) {
				throw new Exception("Unable to find link type : "
						+ linkTypeName);
			} else {
				
				// --- Now we know we found the right link type
				LOG.debug("CreateAndLinkDetail: " + "issue link type is : "
						+ linkTypeValue.getName());
				this.setLinkType(linkTypeValue);
				// --- Link direction is determined
				if (linkTypeName.equals(linkTypeValue.getOutward())) {
					this.setLinkDirection(LinkedIssueConstants.OUTWARD);
				} else {
					this.setLinkDirection(LinkedIssueConstants.INWARD);
				}
			}
		}
		LOG.debug("CreateAndLinkDetails: Link direction:" + getLinkDirection());

		LOG.debug("do execute mappingid : " + mappingid);

		return super.doExecute();
		
		
	}

	/**
	 * Method used to get a new issue form the factory, presetted with entered param.
	 * @param genericValue : genericValue.
	 * @return a preformated issue.
	 */
	public final Issue getIssueObject(final GenericValue genericValue) {
		return issueFactory.getIssue(genericValue);
	}

	/**
	 * Action called automatically by the JIRA api, after a creation of issue.
	 * This action has in chage to : <br>
	 * - Create a link between 2 issues.<br>
	 * - Duplication of attachments if it is configured.<br>
	 * - Add watcher if it is configured.<br>
	 * - Update parent issue's field if it is configured.<br>
	 * - and re-index the issue.<br>
	 * @return the next page : the issue parent detail.
	 * @throws Exception : Exception.
	 */
	protected final String doPostCreationTasks() throws Exception {

		// Now we have both parent and new issue
		Issue parentIssue = getOriginalIssue();
		Issue currentIssue = getIssueObject(getIssue());
		IssueLinkType lt = getLinkType();

		// retrieve the LinkedIssueContextManager.
		LinkedIssueContextManager licm = LinkedIssueContextManager
				.getInstance();

		

			
		// Linking the issues
		LOG.debug("Creating a " + lt.getName() + " link between "
				+ parentIssue.getKey() + " and " + currentIssue.getKey());
	
		if (getLinkDirection().equals(LinkedIssueConstants.OUTWARD)) {
			issueLinkManager.createIssueLink(currentIssue.getId(), parentIssue
					.getId(), lt.getId(), null, getRemoteUser());
		} else {
			issueLinkManager.createIssueLink(parentIssue.getId(), currentIssue
					.getId(), lt.getId(), null, getRemoteUser());
		}
		
		// Duplication of attachements if it is needed.
		if (licm.isDuplicateAttachmentsActivated(parentIssue, mappingid)) {
			duplicatAttachments(parentIssue, currentIssue);
		}

		// Add watchers
		if (licm.isIssueMapped(parentIssue, mappingid)) {
			if (licm.isWatcherToAdd(parentIssue, mappingid)) {
				addWatcherToChildIssue(parentIssue);
			}
		}

		// update fields of parent issue
		if (licm.AreParentFieldsToUpdate(parentIssue, mappingid)) {
			// CustomFields
			HashMap cfIds = licm.getParentCustomFieldsIdsToUpdate(parentIssue,
					mappingid);
			if (cfIds != null && !cfIds.isEmpty()) {
				LOG.debug("cfIds to update = " + cfIds.toString());
				FieldManager fm = (FieldManager) ComponentManager
						.getComponentInstanceOfType(FieldManager.class);
				for (Iterator it = cfIds.keySet().iterator(); it.hasNext();) {
					String id = it.next().toString();
					CustomField cf = fm.getCustomField(id);
					if (cf != null) {
						Options options = cf.getOptions(null, cf
								.getRelevantConfig(parentIssue), null);
						String newValue = null;
						if (options != null) {
							LOG.debug("the cf : " + id
									+ " has options, checks if the new value "
									+ cfIds.get(id) + " is a valid option");
							if (options.getOptionForValue(cfIds.get(id)
									.toString(), null) == null) {
								LOG.error("the option " + cfIds.get(id)
										+ " for the field" + id
										+ " is not valid");
								Collection err = getErrorMessages();
								err
										.add("unable to update field "
												+ cf.getName()
												+ " of the parentIssue,\n the new value "
												+ cfIds.get(id)
												+ " is not a valid option for this field");
								setErrorMessages(err);

							} else {
								Option option = options.getOptionForValue(cfIds
										.get(id).toString(), null);
								newValue = option.getValue();
							}
						} else {
							newValue = cfIds.get(id).toString();
						}
						LOG.debug("parent cf to update : " + id);
						LOG.debug("parent cf old value : "
								+ parentIssue.getCustomFieldValue(cf));
						LOG.debug("parent cf new value : " + newValue);
						if (newValue != null) {
							try {
								cf.updateValue(null, parentIssue,
										new ModifiedValue(parentIssue
												.getCustomFieldValue(cf),
												newValue),
										new DefaultIssueChangeHolder());
							} catch (Exception e) {
								Collection err = getErrorMessages();
								err
										.add("unable to update field : "
												+ cf.getName()
												+ " of the parentIssue, the new value : "
												+ newValue
												+ ", is not valid for this field");
								setErrorMessages(err);
								LOG
										.error("unable to update field of the parentIssue");
								LOG.error(e.getMessage());
							}
						}
					}
				}
			}
			// System fields
			HashMap fieldIds = licm.getParentFieldsToUpdate(parentIssue,
					mappingid);
			if (fieldIds != null && !fieldIds.isEmpty()) {
				LOG.debug("fieldsIds to update = " + fieldIds.toString());
				FieldManager fm = (FieldManager) ComponentManager
						.getComponentInstanceOfType(FieldManager.class);
				IssueManager im = (IssueManager) ComponentManager
						.getComponentInstanceOfType(IssueManager.class);
				for (Iterator it = fieldIds.keySet().iterator(); it.hasNext();) {
					String id = it.next().toString();
					OrderableField field = (OrderableField) fm.getField(id);
					if (field != null) {
						LOG.debug("parent field to update : " + id);
						LOG.debug("parent field new value : "
								+ fieldIds.get(id));
						try {
							MutableIssue mut = im.getIssueObject(parentIssue
									.getId());
							field.updateIssue(null, mut, fieldIds);
							mut.store();
						} catch (Exception e) {
							Collection err = getErrorMessages();
							err.add("unable to update field : " + id
									+ " of the parentIssue, the new value : "
									+ fieldIds.get(id)
									+ ", is not valid for this field");
							setErrorMessages(err);
							LOG
									.error("unable to update field of the parentIssue");
							LOG.error(e.getMessage());
						}
					}
				}
			}
			// reindex the issue
			IssueIndexManager indexManager = ComponentManager.getInstance()
					.getIndexManager();
			try {
				indexManager.reIndex(parentIssue);
			} catch (IndexException e) {
				LOG.error("unable to reindex the issue, " + e.getMessage());
			}
		}
		// Return to the parent issue to make it easier to create
		// and link multiple issues
		if (permissionManager.hasPermission(Permissions.BROWSE, parentIssue,
				getRemoteUser())) {
			return getRedirect("/browse/" + parentIssue.getKey());
		} else {
			return getRedirect("CantBrowseCreatedIssue.jspa?issueKey=" + parentIssue.getKey());
		}
	}
	

	/**
	 * Method used to duplicate the attachments form the parent issue to the
	 * child issue.
	 * 
	 * @param issueParent
	 *            : issue parent.
	 * @param issueChild
	 *            : issue child.
	 */
	private void duplicatAttachments(final Issue issueParent,
			final Issue issueChild) {

		ArrayList attachments = (ArrayList) issueParent.getAttachments();

		if (attachments.size() != 0) {
			for (int i = 0; i < attachments.size(); i++) {
				Attachment att = (Attachment) attachments.get(i);
				File origAttachment = AttachmentUtils.getAttachmentFile(att);
				File newAttachment = new File(attachmentHome + File.separator
						+ att.getFilename());
				try {
					FileUtils.copyFile(origAttachment, newAttachment);
					attachmentManager.createAttachment(newAttachment,
							newAttachment.getName(), att.getMimetype(),
							authenticationContext.getUser(), issueChild
									.getGenericValue());
					LOG.debug("The attachments : " + newAttachment.getName()
							+ " is added to the new issue.");
				} catch (IOException e) {
					log.error("An error occurs : " + e.getMessage());
				} catch (AttachmentException e) {
					log.error("An error occurs : " + e.getMessage());
				} catch (GenericEntityException e) {
					log.error("An error occurs : " + e.getMessage());
				}

			}
		}

	}

	/**
	 * This method is used to add defined watchers to the child issue.
	 * @param parentIssue : the parent Issue.
	 * @throws LinkNewIssueOperationException : LinkNewIssueOperationException.
	 * @throws EntityNotFoundException : EntityNotFoundException.
	 * 
	 */
	private void addWatcherToChildIssue(final Issue parentIssue)
			throws LinkNewIssueOperationException, EntityNotFoundException {
		WatcherToAddItem[] watcherToAddItems = LinkedIssueContextManager
				.getInstance().getWatcherToAddItems(parentIssue, mappingid);
		UserManager userManager = new UserManager();
		for (int i = 0; i < watcherToAddItems.length; i++) {
			Watcher watcher = watcherToAddItems[i].getWatcher();
			String role = LinkedIssueContextManager.getInstance()
					.getWatcherToAddRole(watcher);
			if (role != null && role.length() > 0) {
				if (getFieldValuesHolder().get(role) != null
						&& getFieldValuesHolder().get(role).toString()
								.length() > 0) {
					String userName = getFieldValuesHolder().get(role)
							.toString();
					getWatcherManager().startWatching(
							userManager.getUser(userName),
							getIssueObject().getGenericValue());
				}
			} else {
				String cfId = LinkedIssueContextManager.getInstance()
						.getWatcherToAddCfId(watcher);
				if (getCustomFieldManager().getCustomFieldObject(
						Long.valueOf(cfId)) != null) {
					String[] list = getListOfWatchersToAdd(cfId, parentIssue);
					if (list != null && list.length > 0) {
						for (int j = 0; j < list.length; j++) {
							try {
								if (userManager.getUser(list[j]) != null) {
									getWatcherManager().startWatching(
											userManager.getUser(list[j]),
											getIssueObject().getGenericValue());
								} else {
									LOG.warn("Could not add watcher, "
											+ list[j] + " not a user");
								}
							} catch (Exception e) {
								LOG.warn("Could not add watcher, " + list[j]
										+ " not a user");
							}
						}
					}
				} else {
					LOG.warn("No custom field of id = " + cfId
							+ "No Watchers Added by custom field Id");
				}
			}
		}
	}

	/**
	 * 
	 * @param cfId : custom field ID.
	 * @param parentIssue : Parent Issue ID.
	 * @return an array of users names to add as watcher
	 */
	private String[] getListOfWatchersToAdd(final String cfId, final Issue parentIssue) {
		String[] list = null;
		if (cfId != null && cfId.length() > 0) {
			String stringList = getCustomFieldManager().getCustomFieldObject(
					"customfield_".concat(cfId)).getValue(parentIssue)
					.toString();
			if (stringList != null && stringList != "[]") {
				StringTokenizer st = new StringTokenizer(stringList, ",[]");
				int i = 0;
				list = new String[st.countTokens()];
				while (st.hasMoreTokens()) {
					list[i] = st.nextToken().trim();
					i++;
				}
			}
		}
		return list;
	}

	/**
	 * @return the parent issue
	 */
	public final Issue getOriginalIssue() {
		return originalIssue;
	}

	/**
	 * @param newOriginalIssue
	 *            : issue from which the action has been called.
	 */
	public final void setOriginalIssue(final Issue newOriginalIssue) {
		this.originalIssue = newOriginalIssue;
	}

	/**
	 * @return link type object
	 */
	public final IssueLinkType getLinkType() {
		return linkType;
	}

	/**
	 * @param newLinkType
	 *            : the link type selected.
	 */
	public final void setLinkType(final IssueLinkType newLinkType) {
		this.linkType = newLinkType;
	}

	/**
	 * @return link direction, as a string.
	 */
	public final String getLinkDirection() {
		return new String(linkDirection);
	}

	/**
	 * @param newLinkDir
	 *            : the link direction needed.
	 */
	public final void setLinkDirection(final String newLinkDir) {
		this.linkDirection = new String(newLinkDir);
	}

	public String getLinkTypeName() {
		return linkTypeName;
	}

	public void setLinkTypeName(String linkTypeName) {
		this.linkTypeName = linkTypeName;
	}

	public String getMappingid() {
		return mappingid;
	}

	public void setMappingid(String mappingid) {
		this.mappingid = mappingid;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

}
