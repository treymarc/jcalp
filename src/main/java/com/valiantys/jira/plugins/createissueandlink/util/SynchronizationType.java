package com.valiantys.jira.plugins.createissueandlink.util;

public enum SynchronizationType {
	FIELDS,
	COMMENTS,
	STATUT,
	CF_TO_CF,
	FIELD_TO_CF,
	CF_TO_FIELD,
	FIELD_TO_FIELD,
}
