/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class Mapping.
 * 
 * @version $Revision$ $Date$
 */
public class Mapping implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _id.
     */
    private long _id;

    /**
     * keeps track of state for field: _id
     */
    private boolean _has_id;

    /**
     * Field _isAContext.
     */
    private java.lang.String _isAContext = "true";

    /**
     * Field _isAnAssociatedLink.
     */
    private java.lang.String _isAnAssociatedLink = "false";

    /**
     * Field _label.
     */
    private java.lang.String _label = "default";

    /**
     * Field _name.
     */
    private java.lang.String _name;

    /**
     * Field _noinheritance.
     */
    private java.lang.String _noinheritance = "false";

    /**
     * Field _duplicateAttachements.
     */
    private java.lang.String _duplicateAttachements = "false";

    /**
     * Field _issueParent.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent _issueParent;

    /**
     * Field _issueChild.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild _issueChild;

    /**
     * Field _skippedFields.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.SkippedFields _skippedFields;

    /**
     * Field _watcherToAdd.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.WatcherToAdd _watcherToAdd;

    /**
     * Field _parentFieldsToUpdate.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.ParentFieldsToUpdate _parentFieldsToUpdate;


      //----------------/
     //- Constructors -/
    //----------------/

    public Mapping() {
        super();
        setIsAContext("true");
        setIsAnAssociatedLink("false");
        setLabel("default");
        setNoinheritance("false");
        setDuplicateAttachements("false");
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     */
    public void deleteId(
    ) {
        this._has_id= false;
    }

    /**
     * Returns the value of field 'duplicateAttachements'.
     * 
     * @return the value of field 'DuplicateAttachements'.
     */
    public java.lang.String getDuplicateAttachements(
    ) {
        return this._duplicateAttachements;
    }

    /**
     * Returns the value of field 'id'.
     * 
     * @return the value of field 'Id'.
     */
    public long getId(
    ) {
        return this._id;
    }

    /**
     * Returns the value of field 'isAContext'.
     * 
     * @return the value of field 'IsAContext'.
     */
    public java.lang.String getIsAContext(
    ) {
        return this._isAContext;
    }

    /**
     * Returns the value of field 'isAnAssociatedLink'.
     * 
     * @return the value of field 'IsAnAssociatedLink'.
     */
    public java.lang.String getIsAnAssociatedLink(
    ) {
        return this._isAnAssociatedLink;
    }

    /**
     * Returns the value of field 'issueChild'.
     * 
     * @return the value of field 'IssueChild'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild getIssueChild(
    ) {
        return this._issueChild;
    }

    /**
     * Returns the value of field 'issueParent'.
     * 
     * @return the value of field 'IssueParent'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent getIssueParent(
    ) {
        return this._issueParent;
    }

    /**
     * Returns the value of field 'label'.
     * 
     * @return the value of field 'Label'.
     */
    public java.lang.String getLabel(
    ) {
        return this._label;
    }

    /**
     * Returns the value of field 'name'.
     * 
     * @return the value of field 'Name'.
     */
    public java.lang.String getName(
    ) {
        return this._name;
    }

    /**
     * Returns the value of field 'noinheritance'.
     * 
     * @return the value of field 'Noinheritance'.
     */
    public java.lang.String getNoinheritance(
    ) {
        return this._noinheritance;
    }

    /**
     * Returns the value of field 'parentFieldsToUpdate'.
     * 
     * @return the value of field 'ParentFieldsToUpdate'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.ParentFieldsToUpdate getParentFieldsToUpdate(
    ) {
        return this._parentFieldsToUpdate;
    }

    /**
     * Returns the value of field 'skippedFields'.
     * 
     * @return the value of field 'SkippedFields'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.SkippedFields getSkippedFields(
    ) {
        return this._skippedFields;
    }

    /**
     * Returns the value of field 'watcherToAdd'.
     * 
     * @return the value of field 'WatcherToAdd'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.WatcherToAdd getWatcherToAdd(
    ) {
        return this._watcherToAdd;
    }

    /**
     * Method hasId.
     * 
     * @return true if at least one Id has been added
     */
    public boolean hasId(
    ) {
        return this._has_id;
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     * Sets the value of field 'duplicateAttachements'.
     * 
     * @param duplicateAttachements the value of field
     * 'duplicateAttachements'.
     */
    public void setDuplicateAttachements(
            final java.lang.String duplicateAttachements) {
        this._duplicateAttachements = duplicateAttachements;
    }

    /**
     * Sets the value of field 'id'.
     * 
     * @param id the value of field 'id'.
     */
    public void setId(
            final long id) {
        this._id = id;
        this._has_id = true;
    }

    /**
     * Sets the value of field 'isAContext'.
     * 
     * @param isAContext the value of field 'isAContext'.
     */
    public void setIsAContext(
            final java.lang.String isAContext) {
        this._isAContext = isAContext;
    }

    /**
     * Sets the value of field 'isAnAssociatedLink'.
     * 
     * @param isAnAssociatedLink the value of field
     * 'isAnAssociatedLink'.
     */
    public void setIsAnAssociatedLink(
            final java.lang.String isAnAssociatedLink) {
        this._isAnAssociatedLink = isAnAssociatedLink;
    }

    /**
     * Sets the value of field 'issueChild'.
     * 
     * @param issueChild the value of field 'issueChild'.
     */
    public void setIssueChild(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild issueChild) {
        this._issueChild = issueChild;
    }

    /**
     * Sets the value of field 'issueParent'.
     * 
     * @param issueParent the value of field 'issueParent'.
     */
    public void setIssueParent(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent issueParent) {
        this._issueParent = issueParent;
    }

    /**
     * Sets the value of field 'label'.
     * 
     * @param label the value of field 'label'.
     */
    public void setLabel(
            final java.lang.String label) {
        this._label = label;
    }

    /**
     * Sets the value of field 'name'.
     * 
     * @param name the value of field 'name'.
     */
    public void setName(
            final java.lang.String name) {
        this._name = name;
    }

    /**
     * Sets the value of field 'noinheritance'.
     * 
     * @param noinheritance the value of field 'noinheritance'.
     */
    public void setNoinheritance(
            final java.lang.String noinheritance) {
        this._noinheritance = noinheritance;
    }

    /**
     * Sets the value of field 'parentFieldsToUpdate'.
     * 
     * @param parentFieldsToUpdate the value of field
     * 'parentFieldsToUpdate'.
     */
    public void setParentFieldsToUpdate(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.ParentFieldsToUpdate parentFieldsToUpdate) {
        this._parentFieldsToUpdate = parentFieldsToUpdate;
    }

    /**
     * Sets the value of field 'skippedFields'.
     * 
     * @param skippedFields the value of field 'skippedFields'.
     */
    public void setSkippedFields(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.SkippedFields skippedFields) {
        this._skippedFields = skippedFields;
    }

    /**
     * Sets the value of field 'watcherToAdd'.
     * 
     * @param watcherToAdd the value of field 'watcherToAdd'.
     */
    public void setWatcherToAdd(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.WatcherToAdd watcherToAdd) {
        this._watcherToAdd = watcherToAdd;
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping
     */
    public static com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping) org.exolab.castor.xml.Unmarshaller.unmarshal(com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
