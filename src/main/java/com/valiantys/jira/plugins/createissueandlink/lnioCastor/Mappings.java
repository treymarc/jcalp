/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class Mappings.
 * 
 * @version $Revision$ $Date$
 */
public class Mappings implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _items.
     */
    private java.util.Vector _items;


      //----------------/
     //- Constructors -/
    //----------------/

    public Mappings() {
        super();
        this._items = new java.util.Vector();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * 
     * 
     * @param vMappingsItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMappingsItem(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem vMappingsItem)
    throws java.lang.IndexOutOfBoundsException {
        this._items.addElement(vMappingsItem);
    }

    /**
     * 
     * 
     * @param index
     * @param vMappingsItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void addMappingsItem(
            final int index,
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem vMappingsItem)
    throws java.lang.IndexOutOfBoundsException {
        this._items.add(index, vMappingsItem);
    }

    /**
     * Method enumerateMappingsItem.
     * 
     * @return an Enumeration over all
     * com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem
     * elements
     */
    public java.util.Enumeration enumerateMappingsItem(
    ) {
        return this._items.elements();
    }

    /**
     * Method getMappingsItem.
     * 
     * @param index
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     * @return the value of the
     * com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem
     * at the given index
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem getMappingsItem(
            final int index)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("getMappingsItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }
        
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem) _items.get(index);
    }

    /**
     * Method getMappingsItem.Returns the contents of the
     * collection in an Array.  <p>Note:  Just in case the
     * collection contents are changing in another thread, we pass
     * a 0-length Array of the correct type into the API call. 
     * This way we <i>know</i> that the Array returned is of
     * exactly the correct length.
     * 
     * @return this collection as an Array
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem[] getMappingsItem(
    ) {
        com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem[] array = new com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem[0];
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem[]) this._items.toArray(array);
    }

    /**
     * Method getMappingsItemCount.
     * 
     * @return the size of this collection
     */
    public int getMappingsItemCount(
    ) {
        return this._items.size();
    }

    /**
     * Method isValid.
     * 
     * @return true if this object is valid according to the schema
     */
    public boolean isValid(
    ) {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    /**
     * 
     * 
     * @param out
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void marshal(
            final java.io.Writer out)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, out);
    }

    /**
     * 
     * 
     * @param handler
     * @throws java.io.IOException if an IOException occurs during
     * marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     */
    public void marshal(
            final org.xml.sax.ContentHandler handler)
    throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Marshaller.marshal(this, handler);
    }

    /**
     */
    public void removeAllMappingsItem(
    ) {
        this._items.clear();
    }

    /**
     * Method removeMappingsItem.
     * 
     * @param vMappingsItem
     * @return true if the object was removed from the collection.
     */
    public boolean removeMappingsItem(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem vMappingsItem) {
        boolean removed = _items.remove(vMappingsItem);
        return removed;
    }

    /**
     * Method removeMappingsItemAt.
     * 
     * @param index
     * @return the element removed from the collection
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem removeMappingsItemAt(
            final int index) {
        java.lang.Object obj = this._items.remove(index);
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem) obj;
    }

    /**
     * 
     * 
     * @param index
     * @param vMappingsItem
     * @throws java.lang.IndexOutOfBoundsException if the index
     * given is outside the bounds of the collection
     */
    public void setMappingsItem(
            final int index,
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem vMappingsItem)
    throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._items.size()) {
            throw new IndexOutOfBoundsException("setMappingsItem: Index value '" + index + "' not in range [0.." + (this._items.size() - 1) + "]");
        }
        
        this._items.set(index, vMappingsItem);
    }

    /**
     * 
     * 
     * @param vMappingsItemArray
     */
    public void setMappingsItem(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem[] vMappingsItemArray) {
        //-- copy array
        _items.clear();
        
        for (int i = 0; i < vMappingsItemArray.length; i++) {
                this._items.add(vMappingsItemArray[i]);
        }
    }

    /**
     * Method unmarshal.
     * 
     * @param reader
     * @throws org.exolab.castor.xml.MarshalException if object is
     * null or if any SAXException is thrown during marshaling
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     * @return the unmarshaled
     * com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings
     */
    public static com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings unmarshal(
            final java.io.Reader reader)
    throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings) org.exolab.castor.xml.Unmarshaller.unmarshal(com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mappings.class, reader);
    }

    /**
     * 
     * 
     * @throws org.exolab.castor.xml.ValidationException if this
     * object is an invalid instance according to the schema
     */
    public void validate(
    )
    throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
