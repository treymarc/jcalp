package com.valiantys.jira.plugins.createissueandlink.postFunctions;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutStorageException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.user.User;
import com.opensymphony.workflow.WorkflowException;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.LnioPFCreateIssueAndLink;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.manager.LnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

public class LnioFunction extends AbstractJiraFunctionProvider {

	/**
	 * Logger.
	 */
	protected final static Logger LOG = Logger.getLogger(LnioFunction.class);
	
	/**
	 * Manager of the LNIO Configuration.
	 */
	final LnioConfigurationManager lnioConfigManager;
	
	public LnioFunction(final LnioConfigurationManager lnioConfigManager) {
		this.lnioConfigManager = lnioConfigManager;
	}
	
	public void execute(Map transientVars, Map args, PropertySet ps)
			throws WorkflowException {
//	 A supprimer apr�s de test de non reg.	
//		LNIOPropertiesImpl lnioProperties = new LNIOPropertiesImpl(ComponentManager.getComponentInstanceOfType(JiraPropertySetFactory.class));
//		LnioConfigurationManager lnioConfigManager = new LnioConfigurationManager(lnioProperties);
		
		// Retrieval of the the LinkedIssueContext object (LNIO configuration object)
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		
		String mappingId = (String) args
		.get(LnioFunctionFactory.TARGET_LNIO_PF_MAPPING_ID);

		String enableVerif = (String) args
		.get(LnioFunctionFactory.TARGET_LNIO_PF_CONDITION);
		
		// Retrieval of the parent issue
		Issue parentIssue = (Issue) transientVars.get("issue");
		
		LnioPFCreateIssueAndLink pfLNIOissueCreator = new LnioPFCreateIssueAndLink();
		
		
		
		int matchingLevel = 0;
		
		try {
			
				if (ComponentManager.getInstance().getIssueLinkManager().isLinkingEnabled()) {
				
				// Retrieval of of the mapping object corresponding to the mapping id defined in the post function
				Mapping mapping = licm.getMappingById(mappingId);
				
				// check if the use has right to create an issue in the destination project
				User currentUser = ComponentManager.getInstance().getJiraAuthenticationContext().getUser();
				IssueChildItem[] issueChildItems = mapping.getIssueChild().getIssueChildItem();
				Project destinationProject = null;
				for (int i = 0; i < issueChildItems.length; i++) {
					if (issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_PROJECT) == 0) {
						if ( issueChildItems[i].getParam().getValue().compareTo("CURRENT_PRJ") != 0 ) {
							destinationProject = ComponentManager.getInstance().getProjectManager().getProjectObj(new Long(issueChildItems[i].getParam().getValue()));
						}else{
							destinationProject = parentIssue.getProjectObject();
						}
					}
				}
				if (ComponentManager.getInstance().getPermissionManager().hasPermission(Permissions.CREATE_ISSUE, destinationProject, currentUser)) {	
				
					// check if exists required fields which are not mapped
					if ( !licm.existsRequiredFieldsNotMapped(lnioConfigManager, mapping, parentIssue) ) {
						
						LOG.debug("Pas de champs non mapp�s");
					
						if (mapping != null) {
							
							matchingLevel = licm.checkMatchingIssue(mapping.getIssueParent(), parentIssue);
							
							LOG.debug("Niveau de matching entre mapping.getIssueParent() et parentIssue : " + matchingLevel);
							
							// if the matching level is positive, the mapping is matching the parent issue
							if (matchingLevel > 0) {
							
								// check if the post function condition checkbox has been checked
								if (enableVerif.compareTo("on") == 0) {
									
									// retrieval of issues linked to the parent issue
									List<Issue> existingLinkedIssues = licm.getExistingLinkedIssues(parentIssue, mapping);
									
									if ( existingLinkedIssues.size() > 0 )  {
									
										// check if a linked issue has the same parameters than the issueChild mapping parameters
										if ( licm.existsLinkedIssuesWithSameIssueChildScheme(parentIssue, existingLinkedIssues, mapping) ) {
											
											LOG.debug("There's at least one linked issue, with the same issue child scheme");
											
										}else{
											
											LOG.debug("There's no linked issue with the same issue child scheme");
											
											pfLNIOissueCreator.createAndLinkNewIssue(licm, parentIssue, mapping, lnioConfigManager);
											
										}
									
									}else{
										
										LOG.debug("There's no linked issues to the parent issue");
										
										pfLNIOissueCreator.createAndLinkNewIssue(licm, parentIssue, mapping, lnioConfigManager);
										
									}
									
								}else{
									
									LOG.debug("Verification condition isn't enabled");
									
									pfLNIOissueCreator.createAndLinkNewIssue(licm, parentIssue, mapping, lnioConfigManager);
									
								}
								
							}
							
						}
					
					}else{
						
						LOG.warn("Mandatory fields on the mapping child issue scheme are not mapped in the field by field mapping. The issue can't be created");
						
					}
				
				}else{
					
					LOG.warn("The current user hasn't the good rights to create a new issue in the destination project defined");
					
				}
				
			}else{
				LOG.warn("The issue linking isn't enabled, so the issue isn't created");
			}
			
		} catch (LinkNewIssueOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FieldLayoutStorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
