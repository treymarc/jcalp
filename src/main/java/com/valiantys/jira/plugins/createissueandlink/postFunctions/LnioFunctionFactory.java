package com.valiantys.jira.plugins.createissueandlink.postFunctions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;

public class LnioFunctionFactory extends AbstractWorkflowPluginFactory
		implements WorkflowPluginFunctionFactory {

	
	
	/**
	 * List of entirely child defined mappings
	 */
	public static final String ENTIRELY_CHILD_DEFINED_MAPPINGS = "entirelyChildDefinedMappings";
	
	/**
	 * Parameter : mappingId.
	 */
	public static final String PARAM_MAPPING_ID = "mappingId";

	/**
	 * Parameter : enableVerif.
	 */
	public static final String PARAM_ENABLE_VERFICATION = "enableVerif";
	
	/**
	 * constant used in the descriptor.
	 */
	public static final String TARGET_LNIO_PF_MAPPING_ID = "lnio.pf.mapping.id";

	/**
	 * constant used in the descriptor.
	 */
	public static final String TARGET_LNIO_PF_CONDITION = "lnio.pf.condition";
	
	
	
	
	
	@Override
	protected void getVelocityParamsForEdit(Map<String, Object> velocityParams,
			AbstractDescriptor descriptor) {
		
		this.getVelocityParamsForInput(velocityParams);

		if (!(descriptor instanceof FunctionDescriptor)) {
			throw new IllegalArgumentException(
					"Descriptor must be a FunctionDescriptor.");
		}

		FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
		velocityParams.put(PARAM_MAPPING_ID, functionDescriptor.getArgs().get(TARGET_LNIO_PF_MAPPING_ID));
		velocityParams.put(PARAM_ENABLE_VERFICATION, functionDescriptor.getArgs().get(TARGET_LNIO_PF_CONDITION));

	}

	@Override
	protected void getVelocityParamsForInput(Map velocityParams) {
		
		// Retrieval of the the LinkedIssueContext object (LNIO configuration object)
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		
		// Retrieval of all entirely issue child defined mappings
		List<Mapping> entirelyChildDefinedMappings;
		
		try {
			entirelyChildDefinedMappings = licm.getEntirelyChildDefinedMappings();
			velocityParams.put(ENTIRELY_CHILD_DEFINED_MAPPINGS, entirelyChildDefinedMappings);
		} catch (LinkNewIssueOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void getVelocityParamsForView(Map<String, Object> velocityParams,
			AbstractDescriptor descriptor) {
		if (!(descriptor instanceof FunctionDescriptor)) {
			throw new IllegalArgumentException(
					"Descriptor must be a FunctionDescriptor.");
		} else {
			FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
			String mappingId = (String) functionDescriptor.getArgs().get(TARGET_LNIO_PF_MAPPING_ID);

			velocityParams.put(PARAM_MAPPING_ID, mappingId);
			
			String enableVerif = (String) functionDescriptor.getArgs().get(TARGET_LNIO_PF_CONDITION);

			velocityParams.put(PARAM_ENABLE_VERFICATION, enableVerif);
		}

	}

	public Map<String, ?> getDescriptorParams(Map<String, Object> postFunctionParams) {
		
		Map params = new HashMap();
		String enableVerif = "";

		if (postFunctionParams.containsKey("mappingId")) {
		
			String mappingId = extractSingleParam(postFunctionParams, PARAM_MAPPING_ID);
			params.put(TARGET_LNIO_PF_MAPPING_ID, mappingId);
			
			if (postFunctionParams.containsKey("enableVerif")) {
				enableVerif = extractSingleParam(postFunctionParams, PARAM_ENABLE_VERFICATION);
			}else{
				enableVerif = "off";
			}
			params.put(TARGET_LNIO_PF_CONDITION, enableVerif);
		
		}

		return params;
	}

}
