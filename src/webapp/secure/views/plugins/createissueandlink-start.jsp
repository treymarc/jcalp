<%@ taglib uri="webwork" prefix="webwork" %>
<%@ taglib uri="webwork" prefix="ui" %>
<%@ taglib uri="webwork" prefix="iterator" %>
<%@ taglib uri="sitemesh-page" prefix="page" %>
<html>
<head>
	<title><webwork:text name="'common.words.create'" /> <webwork:text name="'common.words.and'" /> <webwork:text name="'admin.issue.operations.plugin.link.issue.name'" /></title>
</head>
<body>
<webwork:if test="allowedProjects/size > 0">
	<page:applyDecorator name="jiraform">
		<page:param name="title"><webwork:text name="'common.words.create'" /> <webwork:text name="'common.words.and'" /> <webwork:text name="'admin.issue.operations.plugin.link.issue.name'" /></page:param>
		<page:param name="description"> <webwork:text name="'issue.operations.linkexistingissue'" ><webwork:param><webwork:text name="'admin.workflow.function.issue-create.display.name'" /> <webwork:text name="'common.words.and'" /> </webwork:param> <webwork:param> <webwork:property value="/issue/string('key')"/> : <webwork:property value="/issue/string('summary')"/></webwork:param></webwork:text></page:param>
		<page:param name="action">CreateIssueAndLink.jspa</page:param>
        <page:param name="cancelURI"><%= request.getContextPath() %>/browse/<webwork:property value="/issue/string('key')"/></page:param>
		<page:param name="submitName"><webwork:text name="'common.forms.next'" />&gt;&gt;</page:param>

        <webwork:property value="/field('project')/createHtml(null, /, /, /issueObject)" escape="'false'" />
        <webwork:property value="/field('issuetype')/createHtml(null, /, /, /issueObject)" escape="'false'" />
 
        <ui:select name="'linkDesc'" list="linkDescs" listKey="'.'" listValue="'.'" label="text('common.concepts.linkDescription','')">
            <ui:param name="'description'"><webwork:property value="/issue/string('key')"/> : <webwork:property value="/issue/string('summary')"/></ui:param>
        </ui:select>

  <ui:component name="'parentid'" template="hidden.jsp" theme="'single'"/>
  <ui:component name="'mappingid'" template="hidden.jsp" theme="'single'"/>
      
	</page:applyDecorator>
</webwork:if>
<webwork:else>
	<page:applyDecorator name="jiraform">
        <page:param name="title"><webwork:text name="'createissue.title'"/></page:param>
		<page:param name="description"><webwork:text name="'createissue.step1.desc'" /></page:param>
		<%@ include file="/includes/createissue-notloggedin.jsp" %>
	</page:applyDecorator>
</webwork:else>
</body>
</html>